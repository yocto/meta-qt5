require qt5.inc
require qt5-lts.inc

DESCRIPTION = "Qt Quick Timeline"
LICENSE = "GPL-2.0-or-later | The-Qt-Company-Commercial"
LIC_FILES_CHKSUM = " \
    file://LICENSE.GPL2;md5=b234ee4d69f5fce4486a80fdaf4a4263 \
    file://LICENSE.GPL3;md5=d32239bcb673463ab874e80d47fae504 \
    "

DEPENDS = "qtbase qtdeclarative"

SRCREV = "4b85ea25bb125debd9cab3a11b3ecdcdce11ab96"
