LICENSE = "BSD-3-Clause & GPL-3.0-only & The-Qt-Company-GPL-Exception-1.0 | The-Qt-Company-Commercial"
LIC_FILES_CHKSUM = " \
    file://LICENSE.GPL3;md5=d32239bcb673463ab874e80d47fae504 \
    file://LICENSE.FDL;md5=6d9f2a9af4c8b8c3c769f6cc1b6aaf7e \
"

require qt5.inc
require qt5-lts.inc

DEPENDS += "qtbase"

SRCREV = "a8fdb07d8ed8993973765a6e3c0b2ff4e01cc0ad"
